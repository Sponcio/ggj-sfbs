using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VolumeControl : MonoBehaviour
{
    public Slider volumeSlider; // Drag the Slider from your pause panel to the Inspector
    public AudioSource audioSource1; // Drag the first AudioSource from your background music to the Inspector
    public AudioSource audioSource2; // Drag the second AudioSource from another audio element to the Inspector
    public AudioSource audioSource3; // Drag the third AudioSource from yet another audio element to the Inspector

    void Start()
    {
        // Set the Slider value based on the current volume of the first AudioSource
        volumeSlider.value = audioSource1.volume;
    }

    // Method called when the Slider is changed
    public void OnVolumeChange()
    {
        // Set the volume of all three AudioSources based on the Slider position
        audioSource1.volume = volumeSlider.value;
        audioSource2.volume = volumeSlider.value;
        audioSource3.volume = volumeSlider.value;
    }
}






