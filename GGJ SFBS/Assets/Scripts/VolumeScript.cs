using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;


public class VolumeScript : MonoBehaviour
{
    [SerializeField] Slider musicSlider;
    [SerializeField] Slider SFXSlider;
    [SerializeField] Slider masterSlider;
    [SerializeField] AudioMixer myMixer;

    void Start()
    {
        if (PlayerPrefs.HasKey("musicVolume"))
        {
            loadVolume();
        }
        else
        {
            setMusicVolume();
        }

        if (PlayerPrefs.HasKey("SFXVolume"))
        {
            loadSFXVolume();
        }
        else
        {
            setSFXVolume();
        }

        if (PlayerPrefs.HasKey("masterVolume"))
        {
            loadMasterVolume();
        }
        else
        {
            setMasterVolume();
        }


    }
    public void setMusicVolume()
    {
        float volume = musicSlider.value;
        myMixer.SetFloat("music", Mathf.Log10(volume) * 20);
        PlayerPrefs.SetFloat("musicVolume", volume);
    }

    void loadVolume()
    {
        musicSlider.value = PlayerPrefs.GetFloat("musicVolume");
        setMusicVolume();
    }


    public void setSFXVolume()
    {
        float SFXVolume = SFXSlider.value;
        myMixer.SetFloat("SFX", Mathf.Log10(SFXVolume) * 20);
        PlayerPrefs.SetFloat("SFXVolume", SFXVolume);
    }

    void loadSFXVolume()
    {
        SFXSlider.value = PlayerPrefs.GetFloat("SFXVolume");
        setSFXVolume();
    }


    public void setMasterVolume()
    {
        float masterVolume = masterSlider.value;
        myMixer.SetFloat("master", Mathf.Log10(masterVolume) * 20);
        PlayerPrefs.SetFloat("masterVolume", masterVolume);
    }

    void loadMasterVolume()
    {
        masterSlider.value = PlayerPrefs.GetFloat("masterVolume");
        setMasterVolume();
    }

}
