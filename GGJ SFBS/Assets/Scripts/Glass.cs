﻿using TMPro;
using Unity.VisualScripting;
using UnityEngine;

public class Glass : InteractableObject
{
    public TextMeshPro text;
    public GameObject glassBody;
    AudioSource audioSource;

    public GameObject puddle;

    public float timeBeforeSpilling;
    public bool spilled = false;
    float timer;

    private void Awake()
    {
        StartTimer();
        audioSource = GetComponent<AudioSource>();
    }

    private void Update()
    {
        if (timer > 0)
        {
            timer -= Time.deltaTime;
            text.text = Mathf.CeilToInt(timer).ToString();
        }
        else
        {
            if (!spilled)
            {
                spilled = true;
                puddle.SetActive(true);
                GameManager.Instance.glassAlreadyExists = false;
                GameManager.Instance.GlassSpilled();
                Destroy(glassBody);
                text.text = "";
            }
        }
    }

    public void StartTimer()
    {
        timer = timeBeforeSpilling;
    }

    public override void PlayerInteraction()
    {
        if (!spilled)
        {
            GameManager.Instance.PickUpGlass();
            glassBody.SetActive(false);
            Destroy(gameObject);
        }
    }

    private void FixedUpdate()
    {
        if (!spilled)
        {
            transform.position = glassBody.transform.position;
        }
    }
}