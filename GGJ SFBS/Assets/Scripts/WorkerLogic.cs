using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorkerLogic : MonoBehaviour
{
    SpriteRenderer spriteRenderer;
    Animator animator;
    public bool isPenguin;
    public float moveSpeed = 2f;
    public float waitTime = 0f;

    public float offsetHorizontal = 2.0f;
    public float offsetVertical = 1.0f;

    private Vector3 pointA;
    private Vector3 pointB;
    private bool vaVersoB = true;
    float timer;

    private void Start()
    {
        spriteRenderer = GetComponentInChildren<SpriteRenderer>();
        animator = GetComponentInChildren<Animator>();

        pointA = transform.position - new Vector3 (offsetHorizontal, offsetVertical, 0f);
        pointB = transform.position + new Vector3(offsetHorizontal, offsetVertical, 0);
    }

    private void Update()
    {
        if (timer > 0)
        {
            timer -= Time.deltaTime;
            if (timer <= 0 && isPenguin)
            {
                animator.SetTrigger("Going");
            }
            return;
        }

        if (vaVersoB)
        {
            spriteRenderer.flipX = false;
            transform.position = Vector3.MoveTowards(transform.position, pointB, moveSpeed * Time.deltaTime);
            if (transform.position == pointB)
            {
                vaVersoB = false;
                timer = waitTime;
                if (isPenguin)
                    animator.SetTrigger("StandUp");
            }
        }
        else
        {
            spriteRenderer.flipX = true;
            transform.position = Vector3.MoveTowards(transform.position, pointA, moveSpeed * Time.deltaTime);
            if (transform.position == pointA)
            {
                vaVersoB = true;
                timer = waitTime;
                if (isPenguin)
                    animator.SetTrigger("StandUpFlipped");
            }
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawLine(pointA, pointB);
        Gizmos.color = Color.red;
        Gizmos.DrawLine(transform.position - new Vector3(offsetHorizontal, offsetVertical, 0f), transform.position + new Vector3(offsetHorizontal, offsetVertical, 0f));
    }
}
