using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class DialogueFollower : MonoBehaviour
{
    public GameObject whoToFollow;

    public float offset;

    public GameObject my_Buble;

    public TextMeshPro my_text;


    private void Update()
    {
        if (whoToFollow != null)
        {
            transform.position = new Vector2(whoToFollow.transform.position.x, whoToFollow.transform.position.y + offset);
        }
    }

    public void StartBubbleDialogue(GameObject WhoToFollow, float DisplayTime, string WhatToSay, bool IsDarkLord)
    {
        my_Buble.SetActive(true);

        if (IsDarkLord)
        {
            offset = 1.6f;
        }
        else
        {
            offset = 0.3f;
        }

        whoToFollow = WhoToFollow;
        my_text.text = WhatToSay;
        StartCoroutine(DisplayTimeTimer(DisplayTime));
    }

    public void EndBubbleDialogue()
    {
        Destroy(gameObject);
    }

    IEnumerator DisplayTimeTimer(float DisplayTime)
    {
        yield return new WaitForSeconds(DisplayTime);
        EndBubbleDialogue();
    }
}
