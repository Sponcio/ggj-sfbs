using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class DialogueTrigger : MonoBehaviour
{
    public GameObject whoIsTalking;

    public UnityEvent OnDialogStart;

    [TextArea]
    public string whatToSay;

    public float displayTime;

    public bool isDarkLord;

    public bool playerReleaseHere;

    public GameObject nextDialogue;

    Collider2D trigger;

    private void Awake()
    {
        trigger = GetComponent<Collider2D>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            StartDialogue();
        }
    }

    public void StartDialogue()
    {
        GameManager.Instance.StartDialogue(whoIsTalking, displayTime, whatToSay, isDarkLord);

        OnDialogStart?.Invoke();

        if (nextDialogue != null)
        {
            StartCoroutine(DisplayNextDialogue());
        }
    }
    public void DisableCollider()
    {
        trigger.enabled = false;
    }
    public void EnablePlayer()
    {
        GameManager.Instance.StartPlayer();
    }

    IEnumerator DisplayNextDialogue()
    {
        DisableCollider();

        yield return new WaitForSeconds(displayTime + 0.5f);

        if (playerReleaseHere)
        {
            EnablePlayer();
        }

        nextDialogue.SetActive(true);
        nextDialogue.GetComponent<DialogueTrigger>().DisableCollider();
        nextDialogue.GetComponent<DialogueTrigger>().StartDialogue();

        gameObject.SetActive(false);
    }
}
