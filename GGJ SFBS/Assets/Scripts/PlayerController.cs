using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    Rigidbody2D rigidBody;
    [SerializeField] Animator animator;

    Vector2 movementDirection;
    public float normalMovementForce;
    public float slipperyMovementForce;
    public Vector2 lastVelocity;
    [SerializeField] SpriteRenderer spriteRenderer;

    public bool isSlippery;

    private void Awake()
    {
        rigidBody = GetComponentInChildren<Rigidbody2D>();

        if (rigidBody == null)
        {
            Debug.LogError("Missing rigidbody player");
        }
    }

    void Update()
    {
        movementDirection = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical")).normalized;
        animator.SetFloat("Speed", movementDirection.magnitude);
        if (movementDirection.x > 0)
        {
            spriteRenderer.flipX = false;
        }
        else if (movementDirection.x < 0)
        {
            spriteRenderer.flipX = true;
        }
        else 
        {
        
        }
    }

    private void FixedUpdate()
    {
        if (isSlippery)
        {
            rigidBody.AddForce(movementDirection * slipperyMovementForce * Time.fixedDeltaTime, ForceMode2D.Force);

            if (rigidBody.velocity != Vector2.zero)
            {
                lastVelocity = rigidBody.velocity;
            }
        }
        else
        {
            rigidBody.velocity = movementDirection * normalMovementForce;
        }

    }
}