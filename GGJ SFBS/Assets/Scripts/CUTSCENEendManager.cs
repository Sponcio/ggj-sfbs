using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CUTSCENEendManager : MonoBehaviour
{
    Collider2D trigger;

    public GameObject firstDialogue;
    public float silenceIntroDuration;

    public GameObject wheresMyWaterDialogue;

    private void Awake()
    {
        trigger = GetComponent<Collider2D>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            if (GameManager.Instance.playerIsHoldingGlass)
            {
                GameManager.Instance.StopPlayer();

                DisableCollider();

                StartCoroutine(StartDialogueEnd());
            }
            else
            {
                StartCoroutine(StartDialogueWheresMyWater());
            }
        }
    }

    public void DisableCollider()
    {
        trigger.enabled = false;
    }

    IEnumerator StartDialogueEnd()
    {
        yield return new WaitForSeconds(silenceIntroDuration);
        firstDialogue.SetActive(true);
    }

    IEnumerator StartDialogueWheresMyWater()
    {
        yield return new WaitForSeconds(silenceIntroDuration);
        wheresMyWaterDialogue.SetActive(true);
    }
}