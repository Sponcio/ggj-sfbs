using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Sink : InteractableObject
{
    public GameObject puddle;
    bool firstGlass = true;

    public UnityEvent onFirstTimeDialogue;

    public override void PlayerInteraction()
    {
        if (!GameManager.Instance.glassAlreadyExists)
        {
            GameManager.Instance.PickUpGlass();
            if (firstGlass)
            {
                firstGlass = false;
                GameManager.Instance.BringChaosToTheWorld();
                onFirstTimeDialogue.Invoke();
            }
        }

        puddle.SetActive(true);
    }
}