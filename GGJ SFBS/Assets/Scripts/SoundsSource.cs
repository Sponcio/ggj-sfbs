using UnityEngine;

public class SoundsSource : MonoBehaviour
{
    AudioSource audioSource;
    public AudioClip hitSound;
    public AudioClip pickUpSound;

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }

    public void PlaySound(string name)
    {
        switch (name)
        {
            case "hitSound":
                audioSource.PlayOneShot(hitSound);
                break;
            case "pickUpSound":
                audioSource.PlayOneShot(pickUpSound);
                break;
        }
    }
}