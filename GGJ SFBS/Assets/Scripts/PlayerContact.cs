using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerContact : MonoBehaviour
{
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (!collision.collider.CompareTag("SinkBody"))
        {
            GameManager.Instance.PlayerHasCollided();
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.TryGetComponent(out InteractableObject interactableObject))
        {
            interactableObject.PlayerInteraction();
        }
        else if (collision.CompareTag("Room"))
        {
            GameManager.Instance.MoveCameraToRoom(collision.transform);
        }
        else if (collision.CompareTag("SinkRoom"))
        {
            GameManager.Instance.MoveCameraToSinkRoom(collision.transform);
        }
    }
}