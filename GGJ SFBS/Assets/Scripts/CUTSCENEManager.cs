using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CUTSCENEManager : MonoBehaviour
{
    Collider2D trigger;

    public GameObject firstDialogue;
    public float silenceIntroDuration;

    private void Awake()
    {
        trigger = GetComponent<Collider2D>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            GameManager.Instance.StopPlayer();

            DisableCollider();

            StartCoroutine(StartDialogueIntro());
        }
    }

    public void DisableCollider()
    {
        trigger.enabled = false;
    }

    IEnumerator StartDialogueIntro()
    {
        yield return new WaitForSeconds(silenceIntroDuration);
        firstDialogue.SetActive(true);
    }
}