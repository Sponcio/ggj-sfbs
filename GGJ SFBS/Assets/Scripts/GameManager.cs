using System.Collections;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : Singleton<GameManager>
{
    [Header("Li Prende In Automatico")]
    public GameObject player;
    public Transform playerTransform;
    public PlayerController playerController;
    Rigidbody2D playerRb;
    GameObject carriedGlassSprite;

    [Header("Da Mettere Manualmente")]
    public GameObject glassPrefab;
    public GameObject worldBeforeChaos;
    public GameObject worldAfterYouPickedUpAGlassOfWater;
    public SoundsSource soundsSource;

    public GameObject dialogue5SecRule;
    public GameObject dialogueWaterSpilled;

    [SerializeField] float collisionDropVelocityThreshold = .1f;

    public bool playerIsHoldingGlass = false;
    public bool glassAlreadyExists = false;
    bool firstDrop = true;
    bool firstSpill = true;

    [SerializeField] float glassDropDistance = 2;

    [Header("Dialogues")]

    public GameObject dialogueBublePrefab;
    public Transform lastDialoguePosition;

    [Header("Pause Men�")]

    public Canvas pauseCanvas;
    public Canvas exitConfirmationCanvas;

    public Button exitButton;
    public Button quitButton;
    public Button returnToPauseButton;
    public Button resumeButton;
    public Button restartButton; 

    public string restartSceneName;

    private bool exitConfirmationActive = false;
    private bool pauseActive = false;

    protected override void Awake()
    {
        base.Awake();
        player = GameObject.FindGameObjectWithTag("Player");
        playerTransform = player.transform;
        playerController = player.GetComponent<PlayerController>();
        carriedGlassSprite = GameObject.Find("Sprite glass carried");
        carriedGlassSprite.SetActive(false);
        playerRb = player.GetComponentInChildren<Rigidbody2D>();

        exitConfirmationCanvas.gameObject.SetActive(false);

        exitButton.onClick.AddListener(ShowExitConfirmationCanvas);
        quitButton.onClick.AddListener(CloseExitConfirmationCanvas);
        returnToPauseButton.onClick.AddListener(ConfirmExit);
        resumeButton.onClick.AddListener(ClosePauseCanvas);
        restartButton.onClick.AddListener(RestartGame);
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (exitConfirmationActive)
            {
                CloseExitConfirmationCanvas();
            }
            else if (pauseActive)
            {

                ClosePauseCanvas();
            }
            else
            {

                ShowPauseCanvas();
            }
        }
    }

    public void PlayerHasCollided()
    {
        if (playerIsHoldingGlass && playerController.lastVelocity.magnitude > collisionDropVelocityThreshold)
        {
            soundsSource.PlaySound("hitSound");
            DropGlass();
        }
    }

    public void DropGlass()
    {
        if (firstDrop)
        {
            firstDrop = false;
            dialogue5SecRule.SetActive(true);
        }
        playerIsHoldingGlass = false;
        carriedGlassSprite.SetActive(false);
        Vector2 glassDropDestination = (Vector2)playerTransform.position - (glassDropDistance * playerController.lastVelocity.normalized);
        Instantiate(glassPrefab, glassDropDestination, Quaternion.identity);
    }

    public void GlassSpilled()
    {
        if (firstSpill)
        {
            firstSpill = true;
            dialogueWaterSpilled.SetActive(true);
        }
    }

    public void PickUpGlass()
    {
        if (!playerIsHoldingGlass)
        {
            soundsSource.PlaySound("pickUpSound");
            glassAlreadyExists = true;
            playerIsHoldingGlass = true;
            carriedGlassSprite.SetActive(true);
            playerController.isSlippery = true;
        }
    }

    public void StartDialogue(GameObject WhoToFollow, float DisplayTime, string WhatToSay, bool IsDarkLord)
    {
        GameObject bubble = Instantiate(dialogueBublePrefab);
        bubble.GetComponent<DialogueFollower>().StartBubbleDialogue(WhoToFollow, DisplayTime, WhatToSay, IsDarkLord);
    }

    public void MoveCameraToRoom(Transform room)
    {
        Camera.main.transform.position = new Vector3 (room.position.x, room.position.y, -10);
        Camera.main.orthographicSize = 8;
    }

    public void MoveCameraToSinkRoom(Transform room)
    {
        Camera.main.transform.position = new Vector3 (room.position.x, room.position.y, -10);
        Camera.main.orthographicSize = 4;
    }

    public void BringChaosToTheWorld()
    {
        worldBeforeChaos.SetActive(false);
        worldAfterYouPickedUpAGlassOfWater.SetActive(true);
    }

    public void StartPlayer()
    {
        playerController.enabled = true;
    }

    public void StopPlayer()
    {
        playerController.enabled &= false;
    }

    public void StopPlayerFrFr()
    {
        playerController.enabled &= false;
        playerRb.velocity = Vector3.zero;
        playerRb.totalForce = Vector2.zero;
        player.transform.position = lastDialoguePosition.position;
    }

    public void ShowExitConfirmationCanvas()
    {
        pauseCanvas.gameObject.SetActive(false);
        exitConfirmationCanvas.gameObject.SetActive(true);
        exitConfirmationActive = true;
        pauseActive = false;
    }

    public void CloseExitConfirmationCanvas()
    {
        exitConfirmationCanvas.gameObject.SetActive(false);
        pauseCanvas.gameObject.SetActive(true);
        exitConfirmationActive = false;
        pauseActive = true;
    }

    public void ConfirmExit()
    {
        Application.Quit();
    }

    public void ShowPauseCanvas()
    {
        pauseCanvas.gameObject.SetActive(true);
        Time.timeScale = 0;
        pauseActive = true;
    }

    public void ClosePauseCanvas()
    {
        pauseCanvas.gameObject.SetActive(false);
        Time.timeScale = 1;
        pauseActive = false;
    }

    public void RestartGame()
    {
        Time.timeScale = 1;
        UnityEngine.SceneManagement.SceneManager.LoadScene(restartSceneName);
    }

    public void ReturnToMenuAfterFiveSeconds()
    {
        StartCoroutine(BackToMainMenu());
    }

    IEnumerator BackToMainMenu()
    {
        yield return new WaitForSeconds(5);
        UnityEngine.SceneManagement.SceneManager.LoadScene(0);
    }

}