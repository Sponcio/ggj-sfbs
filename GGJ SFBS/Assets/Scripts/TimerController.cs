using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.Events;

public class TimerController : MonoBehaviour
{
    public TextMeshProUGUI Clock;
    public float totalTime;
    public UnityEvent OnTimerStart, OnTimerEnd;

    private void Start()
    {
        Clock.text = "";
    }

    public void StartCountDown()
    {
        Clock.enabled = true;
        StartCoroutine(CountDownRoutine());
    }

    private IEnumerator CountDownRoutine()
    {
        OnTimerStart.Invoke();

        float currentTime = totalTime;

        while (currentTime > 0f)
        {
            currentTime -= Time.deltaTime;
            UpdateClockText(currentTime);
            yield return null;
        }

        currentTime = 0f;
        UpdateClockText(currentTime);
        OnTimerEnd.Invoke();
    }

    private void UpdateClockText(float time)
    {
        float seconds = time;
        int minutes = (int)seconds / 60;
        seconds %= 60;
        if (seconds < 10)
        {
            Clock.text = minutes.ToString() + " : 0" + ((int)seconds).ToString();

        }
        else
        {
            Clock.text = minutes.ToString() + " : " + ((int)seconds).ToString();
        }
    }

    public void StopCountDown()
    {
        StopAllCoroutines();
    }
}

