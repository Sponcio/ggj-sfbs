using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SubMenuButton : BaseButton
{
    [Header("GameObject to activate on click")]
    public List<GameObject> thingsToSetActive;
    [Header("GameObject to disactivate on click")]
    public List<GameObject> thingsToSetInactive;

    protected override void OnClick()
    {
        //Attivare
        foreach (GameObject go in thingsToSetActive)
        {
            go.SetActive(true);
        }

        //Disattivare
        foreach (GameObject go in thingsToSetInactive)
        {
            go.SetActive(false);
        }
    }
}
