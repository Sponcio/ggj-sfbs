using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;


public class LoadSceneButton : BaseButton
{
    public string sceneName;
    protected override void OnClick()
    {
        SceneManager.LoadScene(sceneName);
    }
}
