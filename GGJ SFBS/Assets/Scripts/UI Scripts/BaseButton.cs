using UnityEngine;
using UnityEngine.UI;

public abstract class BaseButton : MonoBehaviour
{
    public Button button;

    protected virtual void Awake()
    {
        button.onClick.AddListener(OnClick);

        if (button == null) 
        {
            button = GetComponent<Button>();
        }
        
    }

    protected virtual void OnDestroy()
    {
        button.onClick.RemoveListener(OnClick);
    }

    protected abstract void OnClick();
}