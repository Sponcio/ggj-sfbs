using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuExitButton : BaseButton
{
    protected override void OnClick()
    {
        Application.Quit();
    }
}
