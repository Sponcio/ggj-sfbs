using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class WashingMachineLogic : MonoBehaviour
{
    public Transform pivot;
    public float rotationSpeed;
    [SerializeField] Transform body;

    private void FixedUpdate()
    {
        body.transform.RotateAround(pivot.position, Vector3.forward, rotationSpeed * Time.fixedDeltaTime);
        body.transform.Rotate(Vector3.forward, -rotationSpeed * Time.fixedDeltaTime);
    }
}